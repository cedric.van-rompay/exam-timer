A hipster timer for exam supervision

# Usage

* edit the HTML file with the exam name, some info and the ending time.
* open it in your web browser
* don't forget you can zoom in/out with your browser with `ctrl +` and `ctrl -`

# Credits

takes advantage of:

* [jQuery Countdown](http://www.keith-wood.name/countdown.html)
* [Twitter Bootstrap](http://getbootstrap.com/)
